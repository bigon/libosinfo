# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-29 13:52+0100\n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: Kirghiz\n"
"Language: ky\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Zanata 3.9.5\n"

#: osinfo/osinfo_avatar_format.c:107
msgid "The allowed mime-types for the avatar"
msgstr ""

#: osinfo/osinfo_avatar_format.c:121
msgid "The required width (in pixels) of the avatar"
msgstr ""

#: osinfo/osinfo_avatar_format.c:138
msgid "The required height (in pixels) of the avatar."
msgstr ""

#: osinfo/osinfo_avatar_format.c:155
msgid "Whether alpha channel is supported in the avatar."
msgstr ""

#: osinfo/osinfo_deployment.c:146
msgid "Operating system"
msgstr ""

#: osinfo/osinfo_deployment.c:161
msgid "Virtualization platform"
msgstr ""

#: osinfo/osinfo_devicelink.c:127
msgid "Target device"
msgstr ""

#: osinfo/osinfo_devicelinkfilter.c:132
msgid "Device link target filter"
msgstr ""

#: osinfo/osinfo_entity.c:134
msgid "Unique identifier"
msgstr ""

#: osinfo/osinfo_firmware.c:133 osinfo/osinfo_image.c:168
#: osinfo/osinfo_media.c:483 osinfo/osinfo_resources.c:169
#: osinfo/osinfo_tree.c:316
msgid "CPU Architecture"
msgstr ""

#: osinfo/osinfo_firmware.c:146
msgid "The firmware type"
msgstr ""

#: osinfo/osinfo_image.c:181
msgid "The image format"
msgstr ""

#: osinfo/osinfo_image.c:194
msgid "The URL to this image"
msgstr ""

#: osinfo/osinfo_install_config_param.c:150
msgid "Parameter name"
msgstr ""

#: osinfo/osinfo_install_config_param.c:166
msgid "Parameter policy"
msgstr ""

#: osinfo/osinfo_install_config_param.c:182
msgid "Parameter Value Mapping"
msgstr ""

#: osinfo/osinfo_install_script.c:212
msgid "URI for install script template"
msgstr ""

#: osinfo/osinfo_install_script.c:224
msgid "Data for install script template"
msgstr ""

#: osinfo/osinfo_install_script.c:236
msgid "Install script profile name"
msgstr ""

#: osinfo/osinfo_install_script.c:248
msgid "Product key format mask"
msgstr ""

#: osinfo/osinfo_install_script.c:258
msgid "Expected path format"
msgstr ""

#: osinfo/osinfo_install_script.c:269
msgid "Expected avatar format"
msgstr ""

#: osinfo/osinfo_install_script.c:279
msgid "The preferred injection method"
msgstr ""

#: osinfo/osinfo_install_script.c:291
msgid "The installation source to be used"
msgstr ""

#: osinfo/osinfo_install_script.c:721
msgid "Unable to create XML parser context"
msgstr ""

#: osinfo/osinfo_install_script.c:729
msgid "Unable to read XSL template"
msgstr ""

#: osinfo/osinfo_install_script.c:735
msgid "Unable to parse XSL template"
msgstr ""

#: osinfo/osinfo_install_script.c:826 osinfo/osinfo_install_script.c:857
#, c-format
msgid "Unable to create XML node '%s'"
msgstr ""

#: osinfo/osinfo_install_script.c:832
msgid "Unable to create XML node 'id'"
msgstr ""

#: osinfo/osinfo_install_script.c:836 osinfo/osinfo_install_script.c:862
msgid "Unable to add XML child"
msgstr ""

#: osinfo/osinfo_install_script.c:909 osinfo/osinfo_install_script.c:919
#: osinfo/osinfo_install_script.c:953
msgid "Unable to set XML root"
msgstr ""

#: osinfo/osinfo_install_script.c:930
msgid "Unable to set 'media' node"
msgstr ""

#: osinfo/osinfo_install_script.c:942
msgid "Unable to set 'tree' node"
msgstr ""

#: osinfo/osinfo_install_script.c:976
msgid "Unable to create XSL transform context"
msgstr ""

#: osinfo/osinfo_install_script.c:981
msgid "Unable to apply XSL transform context"
msgstr ""

#: osinfo/osinfo_install_script.c:986
msgid "Unable to convert XSL output to string"
msgstr ""

#: osinfo/osinfo_install_script.c:1047
#, c-format
msgid "Failed to load script template %s: "
msgstr ""

#: osinfo/osinfo_install_script.c:1063
#, c-format
msgid "Failed to apply script template %s: "
msgstr ""

#: osinfo/osinfo_install_script.c:1121 osinfo/osinfo_install_script.c:2028
#: osinfo/osinfo_install_script.c:2079 osinfo/osinfo_install_script.c:2129
msgid "Failed to apply script template: "
msgstr ""

#: osinfo/osinfo_list.c:130
msgid "List element type"
msgstr ""

#: osinfo/osinfo_loader.c:215
#, c-format
msgid "Expected a nodeset in XPath query %s"
msgstr ""

#: osinfo/osinfo_loader.c:295 osinfo/osinfo_loader.c:438
msgid "Expected a text node attribute value"
msgstr ""

#: osinfo/osinfo_loader.c:591
msgid "Missing device id property"
msgstr ""

#: osinfo/osinfo_loader.c:626
msgid "Missing device link id property"
msgstr ""

#: osinfo/osinfo_loader.c:685
msgid "Missing product upgrades id property"
msgstr ""

#: osinfo/osinfo_loader.c:769
msgid "Missing platform id property"
msgstr ""

#: osinfo/osinfo_loader.c:803
msgid "Missing deployment id property"
msgstr ""

#: osinfo/osinfo_loader.c:813
msgid "Missing deployment os id property"
msgstr ""

#: osinfo/osinfo_loader.c:823
msgid "Missing deployment platform id property"
msgstr ""

#: osinfo/osinfo_loader.c:862 osinfo/osinfo_loader.c:1607
msgid "Missing os id property"
msgstr ""

#: osinfo/osinfo_loader.c:986
msgid "Missing install script id property"
msgstr ""

#: osinfo/osinfo_loader.c:1220
msgid "Missing Media install script property"
msgstr ""

#: osinfo/osinfo_loader.c:1760
msgid "Missing OS install script property"
msgstr ""

#: osinfo/osinfo_loader.c:1824
msgid "Incorrect root element"
msgstr ""

#: osinfo/osinfo_loader.c:1891
msgid "Unable to construct parser context"
msgstr ""

#: osinfo/osinfo_loader.c:1913
msgid "Missing root XML element"
msgstr ""

#: osinfo/osinfo_loader.c:2551
#, c-format
msgid ""
"$OSINFO_DATA_DIR is deprecated, please use $OSINFO_SYSTEM_DIR instead. "
"Support for $OSINFO_DATA_DIR will be removed in a future release\n"
msgstr ""

#: osinfo/osinfo_loader.c:2587 osinfo/osinfo_loader.c:2619
#, c-format
msgid ""
"%s is deprecated, please use %s instead. Support for %s will be removed in a "
"future release\n"
msgstr ""

#: osinfo/osinfo_media.c:496
msgid "The URL to this media"
msgstr ""

#: osinfo/osinfo_media.c:509
msgid "The expected ISO9660 volume ID"
msgstr ""

#: osinfo/osinfo_media.c:522
msgid "The expected ISO9660 publisher ID"
msgstr ""

#: osinfo/osinfo_media.c:535
msgid "The expected ISO9660 application ID"
msgstr ""

#: osinfo/osinfo_media.c:548
msgid "The expected ISO9660 system ID"
msgstr ""

#: osinfo/osinfo_media.c:561 osinfo/osinfo_tree.c:342
msgid "The path to the kernel image"
msgstr ""

#: osinfo/osinfo_media.c:574
msgid "The path to the initrd image"
msgstr ""

#: osinfo/osinfo_media.c:587
msgid "Media provides an installer"
msgstr ""

#: osinfo/osinfo_media.c:600
msgid "Media can boot directly w/o installation"
msgstr ""

#: osinfo/osinfo_media.c:622
msgid "Number of installer reboots"
msgstr ""

#: osinfo/osinfo_media.c:640
msgid "Information about the operating system on this media"
msgstr ""

#: osinfo/osinfo_media.c:662
msgid "Supported languages"
msgstr ""

#: osinfo/osinfo_media.c:674
msgid "Expected ISO9660 volume size, in bytes"
msgstr ""

#: osinfo/osinfo_media.c:694
msgid "Whether the media should be ejected after the installtion process"
msgstr ""

#: osinfo/osinfo_media.c:710
msgid ""
"Whether the media should be used for an installation using install scripts"
msgstr ""

#: osinfo/osinfo_media.c:841
#, c-format
msgid "Install media is not bootable"
msgstr ""

#: osinfo/osinfo_media.c:915
#, c-format
msgid "Failed to read \"%s\" directory record extent: "
msgstr ""

#: osinfo/osinfo_media.c:924
#, c-format
msgid "No \"%s\" directory record extent"
msgstr ""

#: osinfo/osinfo_media.c:1128
msgid "Failed to read supplementary volume descriptor: "
msgstr ""

#: osinfo/osinfo_media.c:1135
#, c-format
msgid "Supplementary volume descriptor was truncated"
msgstr ""

#: osinfo/osinfo_media.c:1202
msgid "Failed to read primary volume descriptor: "
msgstr ""

#: osinfo/osinfo_media.c:1209
#, c-format
msgid "Primary volume descriptor was truncated"
msgstr ""

#: osinfo/osinfo_media.c:1241
#, c-format
msgid "Insufficient metadata on installation media"
msgstr ""

#: osinfo/osinfo_media.c:1276
#, c-format
msgid "Failed to skip %d bytes"
msgstr ""

#: osinfo/osinfo_media.c:1281
#, c-format
msgid "No volume descriptors"
msgstr ""

#: osinfo/osinfo_media.c:1323
msgid "Failed to open file: "
msgstr ""

#: osinfo/osinfo_os.c:158
msgid "Generic Family"
msgstr ""

#: osinfo/osinfo_os.c:174
msgid "Generic Distro"
msgstr ""

#: osinfo/osinfo_os.c:190
msgid "Kernel URL Argument"
msgstr ""

#: osinfo/osinfo_product.c:166 tools/osinfo-query.c:56 tools/osinfo-query.c:80
#: tools/osinfo-query.c:106
msgid "Name"
msgstr ""

#: osinfo/osinfo_product.c:179 tools/osinfo-query.c:54 tools/osinfo-query.c:78
msgid "Short ID"
msgstr ""

#: osinfo/osinfo_product.c:192 tools/osinfo-query.c:64 tools/osinfo-query.c:84
#: tools/osinfo-query.c:98
msgid "Vendor"
msgstr ""

#: osinfo/osinfo_product.c:205 tools/osinfo-query.c:58 tools/osinfo-query.c:82
msgid "Version"
msgstr ""

#: osinfo/osinfo_product.c:218
msgid "Codename"
msgstr ""

#: osinfo/osinfo_product.c:231
msgid "URI of the logo"
msgstr ""

#: osinfo/osinfo_resources.c:185
msgid "CPU frequency in hertz (Hz)"
msgstr ""

#: osinfo/osinfo_resources.c:202
msgid "Number of CPUs"
msgstr ""

#: osinfo/osinfo_resources.c:219
msgid "Amount of Random Access Memory (RAM) in bytes"
msgstr ""

#: osinfo/osinfo_resources.c:236
msgid "Amount of storage space in bytes"
msgstr ""

#: osinfo/osinfo_tree.c:329
msgid "The URL to this tree"
msgstr ""

#: osinfo/osinfo_tree.c:355
msgid "The path to the inirtd image"
msgstr ""

#: osinfo/osinfo_tree.c:368
msgid "The path to the bootable ISO image"
msgstr ""

#: osinfo/osinfo_tree.c:381
msgid "Whether the tree has treeinfo"
msgstr ""

#: osinfo/osinfo_tree.c:394
msgid "The treeinfo family"
msgstr ""

#: osinfo/osinfo_tree.c:407
msgid "The treeinfo variant"
msgstr ""

#: osinfo/osinfo_tree.c:420
msgid "The treeinfo version"
msgstr ""

#: osinfo/osinfo_tree.c:433
msgid "The treeinfo architecture"
msgstr ""

#: osinfo/osinfo_tree.c:449
msgid "Information about the operating system on this tree"
msgstr ""

#: osinfo/osinfo_tree.c:702
msgid "Failed to load .treeinfo|treeinfo content: "
msgstr ""

#: osinfo/osinfo_tree.c:711 osinfo/osinfo_tree.c:801
msgid "Failed to process keyinfo file: "
msgstr ""

#: osinfo/osinfo_tree.c:751 osinfo/osinfo_tree.c:792
msgid "Failed to load .treeinfo|treeinfo file: "
msgstr ""

#: osinfo/osinfo_tree.c:830
msgid "URL protocol is not supported"
msgstr ""

#: osinfo/osinfo_os_variant.c:113
msgid "The name to this variant"
msgstr ""

#: tools/osinfo-detect.c:58 tools/osinfo-detect.c:79
#, c-format
msgid "Invalid value '%s'"
msgstr ""

#: tools/osinfo-detect.c:91
msgid "Output format. Default: plain"
msgstr ""

#: tools/osinfo-detect.c:92
msgid "plain."
msgstr ""

#: tools/osinfo-detect.c:95
msgid "The type to be used. Default: media"
msgstr ""

#: tools/osinfo-detect.c:96
msgid "media|tree."
msgstr ""

#: tools/osinfo-detect.c:103
#, c-format
msgid "Media is bootable.\n"
msgstr ""

#: tools/osinfo-detect.c:105
#, c-format
msgid "Media is not bootable.\n"
msgstr ""

#: tools/osinfo-detect.c:136
#, c-format
msgid "Media is an installer for OS '%s (%s)'\n"
msgstr ""

#: tools/osinfo-detect.c:138
#, c-format
msgid "Media is live media for OS '%s (%s)'\n"
msgstr ""

#: tools/osinfo-detect.c:143
#, c-format
msgid "Available OS variants on media:\n"
msgstr ""

#: tools/osinfo-detect.c:185
#, c-format
msgid "Tree is an installer for OS '%s (%s)'\n"
msgstr ""

#: tools/osinfo-detect.c:190
#, c-format
msgid "Available OS variants on tree:\n"
msgstr ""

#: tools/osinfo-detect.c:217
msgid "- Detect if media is bootable and the relevant OS and distribution."
msgstr ""

#: tools/osinfo-detect.c:221 tools/osinfo-query.c:412
#, c-format
msgid "Error while parsing commandline options: %s\n"
msgstr ""

#: tools/osinfo-detect.c:239 tools/osinfo-install-script.c:427
#: tools/osinfo-query.c:428
#, c-format
msgid "Error loading OS data: %s\n"
msgstr ""

#: tools/osinfo-detect.c:255
#, c-format
msgid "Error parsing media: %s\n"
msgstr ""

#: tools/osinfo-detect.c:271
#, c-format
msgid "Error parsing installer tree: %s\n"
msgstr ""

#: tools/osinfo-install-script.c:79
#, c-format
msgid "Expected configuration key=value"
msgstr ""

#: tools/osinfo-install-script.c:144
msgid "Install script profile"
msgstr ""

#: tools/osinfo-install-script.c:146
msgid "Install script output directory"
msgstr ""

#: tools/osinfo-install-script.c:148
msgid "The output filename prefix"
msgstr ""

#: tools/osinfo-install-script.c:150
msgid "The installation source to be used with the script"
msgstr ""

#: tools/osinfo-install-script.c:153
msgid "Set configuration parameter"
msgstr ""

#: tools/osinfo-install-script.c:156
msgid "Set configuration parameters"
msgstr ""

#: tools/osinfo-install-script.c:158
msgid "List configuration parameters"
msgstr ""

#: tools/osinfo-install-script.c:160
msgid "List install script profiles"
msgstr ""

#: tools/osinfo-install-script.c:162
msgid "List supported injection methods"
msgstr ""

#: tools/osinfo-install-script.c:164
msgid "Do not display output filenames"
msgstr ""

#: tools/osinfo-install-script.c:221
#, c-format
msgid "No install script for profile '%s' and OS '%s'"
msgstr ""

#: tools/osinfo-install-script.c:237
msgid "required"
msgstr ""

#: tools/osinfo-install-script.c:237
msgid "optional"
msgstr ""

#: tools/osinfo-install-script.c:323
#, c-format
msgid "No install script for profile '%s' and OS '%s'\n"
msgstr ""

#: tools/osinfo-install-script.c:359
#, c-format
msgid "Unable to generate install script: %s\n"
msgstr ""

#: tools/osinfo-install-script.c:396
msgid "- Generate an OS install script"
msgstr ""

#: tools/osinfo-install-script.c:399
#, c-format
msgid "Error while parsing options: %s\n"
msgstr ""

#: tools/osinfo-install-script.c:417
msgid ""
"Only one of --list-profile, --list-config and --list-injection-methods can "
"be requested"
msgstr ""

#: tools/osinfo-install-script.c:446
#, c-format
msgid "Error finding OS: %s\n"
msgstr ""

#: tools/osinfo-query.c:60
msgid "Family"
msgstr ""

#: tools/osinfo-query.c:62
msgid "Distro"
msgstr ""

#: tools/osinfo-query.c:66 tools/osinfo-query.c:86
msgid "Release date"
msgstr ""

#: tools/osinfo-query.c:68 tools/osinfo-query.c:88
msgid "End of life"
msgstr ""

#: tools/osinfo-query.c:70 tools/osinfo-query.c:90
msgid "Code name"
msgstr ""

#: tools/osinfo-query.c:72 tools/osinfo-query.c:92 tools/osinfo-query.c:112
#: tools/osinfo-query.c:118
msgid "ID"
msgstr ""

#: tools/osinfo-query.c:100
msgid "Vendor ID"
msgstr ""

#: tools/osinfo-query.c:102
msgid "Product"
msgstr ""

#: tools/osinfo-query.c:104
msgid "Product ID"
msgstr ""

#: tools/osinfo-query.c:108
msgid "Class"
msgstr ""

#: tools/osinfo-query.c:110
msgid "Bus"
msgstr ""

#: tools/osinfo-query.c:150 tools/osinfo-query.c:188
#, c-format
msgid "Unknown property name %s"
msgstr ""

#: tools/osinfo-query.c:174
msgid "Syntax error in condition, expecting KEY=VALUE"
msgstr ""

#: tools/osinfo-query.c:400
msgid "Sort column"
msgstr ""

#: tools/osinfo-query.c:402
msgid "Display fields"
msgstr ""

#: tools/osinfo-query.c:407
msgid "- Query the OS info database"
msgstr ""

#: tools/osinfo-query.c:419
#, c-format
msgid "Missing data type parameter\n"
msgstr ""

#: tools/osinfo-query.c:447
#, c-format
msgid "Unknown type '%s' requested\n"
msgstr ""

#: tools/osinfo-query.c:452
#, c-format
msgid "Unable to construct filter: %s\n"
msgstr ""

#: tools/osinfo-query.c:457
#, c-format
msgid "Unable to set field visibility: %s\n"
msgstr ""
